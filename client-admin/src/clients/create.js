import React from 'react';
import {Create, required, SimpleForm, TextInput} from 'react-admin';

export default (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="nombre" validate={[required()]} />
      <TextInput source="telefono" options={{multiLine: true}} validate={[required()]} />
      <TextInput source="direccion" options={{multiLine: true}} validate={[required()]} />
    </SimpleForm>
  </Create>
);
