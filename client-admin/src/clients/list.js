import React from 'react';
import { List, Datagrid, TextField } from 'react-admin';

export default (props) => (
    <List {...props} title="Clientes">
        <Datagrid>
            <TextField source="id" />
            <TextField source="nombre" />
            <TextField source="telefono" />
            <TextField source="direccion" />
        </Datagrid>
    </List>
);
