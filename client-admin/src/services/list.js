import React from 'react';
import { List, Datagrid, TextField, DateField } from 'react-admin';

export default (props) => (
    <List {...props} title="Servicios" >
        <Datagrid rowClick="show">
            <TextField source="id" />
            <TextField source="descripcion" />
            <DateField source="fecha_solicitud" showTime />
            <DateField source="fecha_fin" showTime />
            <DateField source="fecha_inicio" showTime />
            <TextField source="estado" />
            <TextField source="cliente.nombre" />
        </Datagrid>
    </List>
);
