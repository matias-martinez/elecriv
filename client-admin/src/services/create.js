import React, {Component} from 'react';
import {Create, DateTimeInput, ReferenceInput, required, SelectInput, SimpleForm, TextInput} from 'react-admin';

export default class CreateService extends Component {


  render() {
    return (<Create {...this.props}>
      <SimpleForm>
        <DateTimeInput source="fecha_solicitud" validate={[required()]}/>
        <TextInput source="descripcion" options={{multiLine: true}} validate={[required()]}/>
        <ReferenceInput label="Cliente" source="cliente" reference="clientes">
          <SelectInput optionText="nombre"/>
        </ReferenceInput>
      </SimpleForm>
    </Create>);
  }
}
