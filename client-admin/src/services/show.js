import React, {Component} from 'react';
import {DateField, EditButton, NumberField, Query, Show, SimpleShowLayout, TextField} from 'react-admin';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';

const cardActionStyle = {
  zIndex: 2,
  display: 'inline-block',
  float: 'right',
};

const ShowServicioActions = ({basePath, data, onAssign}) => (
  <CardActions style={cardActionStyle}>
    <EditButton basePath={basePath} record={data}/>
    <Button color="primary" onClick={onAssign}>Asignar</Button>
  </CardActions>
);

const payload = {
  pagination: {page: 1, perPage: 10},
  sort: {field: 'username', order: 'ASC'},
};

const CuadrillasList = () => (
  <Query type="GET_LIST" resource="cuadrillas" payload={payload}>
    {({data, total, loading, error}) => {
      if (error) {
        return <p>ERROR</p>;
      }
      return (
        <div>
          <ul>
            {data && data.map(cuadrilla => <li key={cuadrilla.id}>{cuadrilla.nombre}</li>)}
          </ul>
        </div>
      );
    }}
  </Query>
);

export default class ShowServicio extends Component {
  state = {
    assigning: false
  };

  assign = () => {
    this.setState({assigning: true})
  };

  render = () => {

    return (<Show {...this.props} actions={<ShowServicioActions onAssign={this.assign}/>}>
      <SimpleShowLayout>
        <NumberField source="id"/>
        <TextField source="descripcion"/>
        <TextField source="estado"/>
        <TextField source="cliente.nombre"/>
        <TextField source="cuadrilla.nombre"/>
        <DateField source="fecha_solicitud" showTime/>
        <DateField source="fecha_inicio"/>
        <DateField source="fecha_fin"/>
        <Dialog open={this.state.assigning}>
          <DialogTitle>Asignar cuadrilla</DialogTitle>
          <CuadrillasList></CuadrillasList>
        </Dialog>
      </SimpleShowLayout>
    </Show>);
  }
}
