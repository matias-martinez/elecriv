/**
 * Auth provider for ReactADMIN
 */
import { AUTH_LOGIN, AUTH_CHECK, AUTH_LOGOUT } from 'react-admin';
import axios from 'axios';


export default (type, params) => {
    if (type === AUTH_LOGIN) {
        const { username, password } = params;
        return axios.post('/api/auth/login/', {user: username, password}, {withCredentials: false})
            .then((response) => {
                localStorage.setItem('session', JSON.stringify(response.data));
                localStorage.setItem('logged', true);
            })
            .catch(error => {
                throw new Error(error.statusText)
            } );
    }
    if (type === AUTH_CHECK) {
        return localStorage.getItem('logged') ? Promise.resolve() : Promise.reject();
    }
    if (type === AUTH_LOGOUT) {
        localStorage.removeItem('logged');
        localStorage.removeItem('session');
        return Promise.resolve();
    }
    return Promise.resolve();
}