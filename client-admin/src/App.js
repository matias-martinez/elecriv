import React, { Component } from 'react';
import { Admin, Resource, fetchUtils } from 'react-admin';
import spanishMessages from 'aor-language-spanish';

import djangoRestFrameworkProvider from './providers/djangoRestFrameworkProvider';
import authProvider from './providers/authProvider';

import ClientsList from './clients/list';
import CreateClient from './clients/create';
import ServicesList from './services/list';
import CreateServicio from './services/create';
import ShowServicio from './services/show';

// React Admin stuff
const httpClient = (url, options = {}) => {
    if (!options.headers) {
        options.headers = new Headers({ Accept: 'application/json' });
    }
    options.credentials = 'include';
    return fetchUtils.fetchJson(url, options);
};
const API_BASE = process.env.REACT_APP_API_BASE || 'http://localhost:8000/api';
const dataProvider = djangoRestFrameworkProvider(API_BASE, httpClient);

// i18n.
const messages = {
  'es': spanishMessages,
};

const i18nProvider = locale => messages[locale];

class App extends Component {
  render() {
    return (
      <div>
          <Admin locale="es" i18nProvider={i18nProvider} dataProvider={dataProvider} authProvider={authProvider}>
              <Resource name="clientes" list={ClientsList} create={CreateClient} />
              <Resource name="servicios" list={ServicesList} create={CreateServicio} show={ShowServicio} />
              <Resource name="cuadrillas" />
          </Admin>
      </div>
    );
  }
}

export default App;
