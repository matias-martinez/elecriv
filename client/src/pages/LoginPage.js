import React from 'react';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import { grey500, white } from 'material-ui/styles/colors';
import TextField from 'material-ui/TextField';
import Snackbar from 'material-ui/Snackbar';



import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { actionCreators as SessionActions } from '../ducks/SessionDuck';


class LoginPage extends React.Component {

    styles = {
        loginContainer: {
            minWidth: 320,
            maxWidth: 400,
            height: 'auto',
            position: 'absolute',
            top: '20%',
            left: 0,
            right: 0,
            margin: 'auto',
            textAlign: 'center'
        },
        paper: {
            padding: 20,
            overflow: 'auto'
        },
        buttonsDiv: {
            textAlign: 'center',
            padding: 10
        },
        flatButton: {
            color: grey500
        },
        loginBtn: {
            float: 'right'
        },
        btn: {
            background: '#4f81e9',
            color: white,
            padding: 7,
            borderRadius: 2,
            margin: 2,
            fontSize: 13
        },
    };

    constructor(props) {
        super(props);
        this.state = {
            data: {}
        };
        this.props.sessionActions.sessionCheck();
        this.login = this.login.bind(this);
        this.handleFieldChange = this.handleFieldChange.bind(this);
    }

    handleFieldChange(e) {
        const data = this.state.data;
        data[e.target.id] = e.target.value;
        this.setState(data);
    }

    login() {
        this.props.sessionActions.login(this.state.user, this.state.password)
    }

    render() {
        return (
            <div>
                <div style={this.styles.loginContainer}>
                    <h2>Electrónica Rivadavia SRL</h2>
                    <Paper style={this.styles.paper}>

                        <form>
                            <TextField
                                hintText="E-mail"
                                floatingLabelText="E-mail"
                                id="user"
                                onChange={this.handleFieldChange}
                                fullWidth={true}
                            />
                            <TextField
                                hintText="Password"
                                floatingLabelText="Password"
                                id="password"
                                fullWidth={true}
                                onChange={this.handleFieldChange}
                                type="password"
                            />

                            <div>

                                <RaisedButton label="Login"
                                    onClick={this.login}
                                    primary={true}
                                    style={this.styles.loginBtn} />
                            </div>
                        </form>
                    </Paper>
                </div>
                <Snackbar
                    open={this.props.hasError}
                    message="Verifica tu usuario y tu clave"
                    autoHideDuration={4000}
                />
            </div>
        );

    }


};

function mapStateToProps(state) {
    return {
        hasError: state.session.hasError
    }
}

function mapDispatchToProps(dispatch) {
    return {
        navigate: bindActionCreators(push, dispatch),
        sessionActions: bindActionCreators(SessionActions, dispatch)
    };
}

// Default required PROPS definition
LoginPage.defaultProps = {
    error: false
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
