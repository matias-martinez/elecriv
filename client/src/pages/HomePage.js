import React from 'react';

import PageLayout from '../components/PageLayout';
import MenuService from '../utils/MenuService';

import Paper from 'material-ui/Paper';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { actionCreators as SessionActions } from '../ducks/SessionDuck';


class HomePage extends React.Component {

    styles = {
        paper: {
            height: 100,
            width: 100,
            margin: 20,
            textAlign: 'center',
            display: 'inline-block',
            cursor: 'pointer'
        }

    };

    renderContent() {
        return (
            <div>
                <h4 style={{marginLeft: '20px'}}> Bienvenido!</h4>
                {
                    MenuService.getMenuItems().map(item => {
                        return <Paper style={this.styles.paper} 
                                key={item.title}
                                onClick={()=>this.props.navigate(item.target)}
                                zDepth={2} rounded={false}>
                            <div style={{margin: '16px auto'}}>
                                {item.icon}
                            </div>
                            <div>
                                {item.title}

                            </div>
                        </Paper>
                    })
                }

            </div>
        )
    }

    render() {

        return (
            <PageLayout {...this.props}
                navigation={this.navigation}
                requiresAuthentication={true}
                requiresClient={true}
                isLoading={this.props.isLoading}
                hasError={this.props.hasError}>

                {this.renderContent()}

            </PageLayout>
        );
    }


};

function mapStateToProps(state) {
    return {
        hasError: state.session.hasError,
        user: state.session.session
    }
}

function mapDispatchToProps(dispatch) {
    return {
        navigate: bindActionCreators(push, dispatch),
        sessionActions: bindActionCreators(SessionActions, dispatch)
    };
}

// Default required PROPS definition
HomePage.defaultProps = {
    error: false
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
