import React from 'react';

import PageLayout from '../components/PageLayout';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { actionCreators as servicesActions } from '../ducks/ServiceDuck';


class CreateServicesPage extends React.Component {

    constructor(props) {
        super(props);
        this.formConfig = {
            fields:
                [
                    { title: 'Nombre', property: 'descripcion', required: true },
                    { title: 'Cliente', property: 'cliente', required: true },
                    { title: 'Fecha', property: 'fecha_solicitud' }
                ]

        };
        this.navigation = 'Servicios | Nuevo';
    }

    renderContent() {
        return (
            <div>
               <form>
                   
               </form>
            </div>
        )
    }

    render() {

        return (
            <PageLayout {...this.props}
                navigation={this.navigation}
                requiresAuthentication={true}
                requiresClient={true}
                isLoading={this.props.isLoading}
                hasError={this.props.hasError}>
                {this.renderContent()}
            </PageLayout>
        );
    }


};

function mapStateToProps(state) {
    return {
        isLoading: state.service.isLoading
    }
}

function mapDispatchToProps(dispatch) {
    return {
        navigate: bindActionCreators(push, dispatch),
        servicesActions: bindActionCreators(servicesActions, dispatch)
    };
}

// Default required PROPS definition
CreateServicesPage.defaultProps = {
    error: false,
    service: null
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateServicesPage);
