import React from 'react';

import PageLayout from '../components/PageLayout';
import DataTable from '../components/common/DataTable';

import RaisedButton from 'material-ui/RaisedButton';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { actionCreators as servicesActions } from '../ducks/ServiceDuck';


class ServicesPage extends React.Component {

    styles = {
        actionButtonRow: {
            marginBottom: '10px'
        }
    };

    constructor(props) {
        super(props);
        this.dataTableConfig = {
            columns: [{ title: 'Nombre', property: 'descripcion' },
            { title: 'Cliente', property: 'cliente.nombre' },
            { title: 'Fecha', property: 'fecha_solicitud' }],
            actions: ['Edit'],
            selectable: false,
            actionsProperty: 'actions'
        };
        this.navigation = 'Servicios';
    }

    componentWillMount() {
        this.props.servicesActions.getAll();
    }

    renderContent() {
        return (
            <div>
                <div style={this.styles.actionButtonRow}>
                    <RaisedButton primary={true} label='Crear' onClick={()=>this.props.navigate('/services/add')} />
                </div>
                <DataTable config={this.dataTableConfig}
                    data={this.props.services} />
            </div>
        )
    }

    render() {

        return (
            <PageLayout {...this.props}
                navigation={this.navigation}
                requiresAuthentication={true}
                requiresClient={true}
                isLoading={this.props.isLoading}
                hasError={this.props.hasError}>
                {this.renderContent()}
            </PageLayout>
        );
    }


};

function mapStateToProps(state) {
    return {
        user: state.session.session,
        isLoading: state.service.isLoading,
        services: state.service.services
    }
}

function mapDispatchToProps(dispatch) {
    return {
        navigate: bindActionCreators(push, dispatch),
        servicesActions: bindActionCreators(servicesActions, dispatch)
    };
}

// Default required PROPS definition
ServicesPage.defaultProps = {
    error: false,
    services: []
};

export default connect(mapStateToProps, mapDispatchToProps)(ServicesPage);
