import axios from 'axios';

/**
 * Class in charge of
 * handling security on the app
 */
export default class AuthService {

    /**
     * Login the user
     * and save the session
     * @param token
     * @returns {Promise}
     */
    static login(user, pass) {
        return axios.post('/auth/login/', {user: user, password: pass})
             .then(response => {
                localStorage.setItem('session', JSON.stringify(response.data));
                localStorage.setItem('logged', true);  
            })
            .then(() => {return this.getSessionInfo();});
    }

    static updateSession(data) {
        localStorage.setItem('session', JSON.stringify(data));  
    }

    static getSessionInfo() {
        return {
            session: this.getSession(),
            logged: this.isLoggedIn(),
        }
    }

    /**
     * Logout the current user
     * @returns {boolean}
     */
    static logout() {
        localStorage.setItem('logged', false);
        return localStorage.removeItem('session');
    }

    /**
     * Utility function to know
     * if the user is logged in or not
     * @returns {boolean}
     */
    static isLoggedIn() {
        return this.getSession() !== null;
    }

    /**
     * Utility function to obtain the stored session
     * @returns {boolean}
     */
    static getSession() {
        return JSON.parse(localStorage.getItem('session'));
    }

}