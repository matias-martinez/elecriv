import axios from 'axios';

export default class API {

    call(method, endpoint, data = {}) {
        const requestConfiguration = {
            headers: {
                'Content-Type': 'application/json'
            },
            method: method
        };
        if (method === 'delete') {
            requestConfiguration.data = data;
            requestConfiguration.method = 'delete';
            requestConfiguration.url = endpoint;
            return axios(requestConfiguration);
        }
        return axios(endpoint, data, requestConfiguration);
    }


}