import AuthService from './AuthService';
import menuItems from '../menu';
/**
 * Class in charge of
 * handling security on the app
 */
export default class MenuService {

    /**
     * return items that the current user can see
     */
    static getMenuItems() {

        if (AuthService.isLoggedIn()){
            return menuItems.filter(item => {
                return (item.roles.includes(AuthService.getSession().role));
            });
        }
        return [];
        
    }
}