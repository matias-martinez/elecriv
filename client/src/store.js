/**
 * Configure the redux store
 */
import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import { routerMiddleware } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';
import { combineReducers } from 'redux';

/**
 * App reducers
 */
import session from './ducks/SessionDuck';
import service from './ducks/ServiceDuck';


const history = createHistory();

const reducers = combineReducers({
    session,
    service
});

// Middlewares
const middleware = [
    routerMiddleware(history),
    thunk
];

export default function configureStore(initialState) {
    const store = createStore(
        reducers,
        window.devToolsExtension
            ? compose(applyMiddleware(...middleware), window.devToolsExtension())
            : applyMiddleware(...middleware)
    );

    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept(reducers, () => {
            const nextRootReducer = reducers.default;
            store.replaceReducer(nextRootReducer);
        });
    }

    return { store: store, history: history };
}
