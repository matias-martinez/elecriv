import React, { Component } from 'react';
import './App.css';

import { Route } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';

import { Admin, Resource } from 'react-admin';
import simpleRestProvider from 'ra-data-simple-rest';

// Routes Pages
import LoginPage from './pages/LoginPage';
import HomePage from './pages/HomePage';
import ServicesPage from './pages/ServicesPage';
import CreateServicesPage from './pages/CreateServicesPage';

const RealAdmin = () => (
    <Admin dataProvider={simpleRestProvider('http://localhost:8080')}>
        <Resource name="clientes" />
    </Admin>
);

class App extends Component {
  render() {
    return (
      <ConnectedRouter history={this.props.history} >
        <div>
          <Route exact path="/" component={LoginPage} />
          <Route exact path="/admin" component={RealAdmin}></Route>
          <Route exact path="/home" component={HomePage} />
          <Route exact path="/services" component={ServicesPage} />
          <Route exact path="/services/add" component={CreateServicesPage} />
        </div>
      </ConnectedRouter>
    );
  }
}

export default App;
