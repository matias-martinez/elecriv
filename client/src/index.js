import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { Provider } from 'react-redux';
import configureStore from './store';
import { blue600, grey900, white } from 'material-ui/styles/colors';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import axios from 'axios';
import env from './env';

axios.defaults.baseURL = env[process.env.NODE_ENV].BASE_URL;

/**
 * To support Custom baseURL
 */
if (process.env.REACT_APP_BASE_URL && process.env.REACT_APP_BASE_URL !== '') {
    axios.defaults.baseURL = process.env.REACT_APP_BASE_URL;
}

const themeDefault = getMuiTheme({
    palette: {
    },
    appBar: {
        height: 57,
        color: white
    },
    drawer: {
        width: 230,
        color: grey900
    },
    raisedButton: {
        primaryColor: blue600,
    }
});


// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

const storeConfig = configureStore();
const store = storeConfig.store;
const history = storeConfig.history;

const AppContainer = () => {
    return (
        <Provider store={store}>
            <MuiThemeProvider muiTheme={themeDefault}>
                <App history={history} />
            </MuiThemeProvider>
        </Provider>)
};
ReactDOM.render(<AppContainer />, document.getElementById('root'));
registerServiceWorker();
