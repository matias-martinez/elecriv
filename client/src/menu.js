import React from 'react';
import BuildIcon from 'material-ui/svg-icons/action/build';
import HomeIcon from 'material-ui/svg-icons/action/home';
import SecurityIcon from 'material-ui/svg-icons/hardware/security';
import HourglassEmptyIcon from 'material-ui/svg-icons/action/hourglass-empty';


export default [
    {
        "title": "Principal",
        "icon": <HomeIcon />,
        "target": "/home",
        "roles": ["Admin", "Administracion", "Coordinador", "Operario"]        
    },
    {
        "title": "Servicios",
        "icon": <BuildIcon />,
        "target": "/services",
        "roles": ["Admin", "Administracion", "Coordinador", "Operario"]
    },
    {
        "title": "Permisos",
        "icon": <SecurityIcon />,
        "target": "/permits",
        "roles": ["Admin", "Administracion", "Coordinador"]
    },
    {
        "title": "Asistencia",
        "icon": <HourglassEmptyIcon />,
        "target": "/permits",
        "roles": ["Admin", "Administracion", "Coordinador", "operario"]
    }
]