import React, { Component } from 'react';
import IconButton from 'material-ui/IconButton';
import MenuItem from 'material-ui/MenuItem';
import IconMenu from 'material-ui/IconMenu';
import Paper from 'material-ui/Paper';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';

import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';

import { get } from 'lodash';


class DataTable extends Component {

    /**
     * 
     * @param {*} dataItem The oject we want to extract props from
     * @param {*} columnsDefinition Array holding columns to show. cannot be null.
     */
    buildRowColumns(dataItem, columnsDefinition) {
        const rowCols = columnsDefinition.map(columnDef => {
            return <TableRowColumn key={columnDef.property}>
                {get(dataItem, columnDef.property)}
            </TableRowColumn>
        });
        return rowCols;
    }

    /**
     * 
     * @param {*} dataItem The object we want to extract props from
     * @param {*} actionsProperty Name of prop that holds an Array holding actions to show. cannot be null.
     */
    buildOptionsMenu(dataItem, actionsProperty) {
        const menuOptions = get(dataItem, actionsProperty).map(optionDef => {
            return <MenuItem primaryText={optionDef.title} key={dataItem.id || Math.random()} onClick={(dataItem) => optionDef.handler} />
        });
        return menuOptions;
    }

    render() {

        return (
            <Paper>
                <Table selectable={this.props.config.selectable}>
                    <TableHeader displaySelectAll={this.props.config.selectable}
                        adjustForCheckbox={this.props.config.selectable}>
                        <TableRow>
                            {
                                this.props.config.columns.map(columnDef => {
                                    return <TableHeaderColumn key={columnDef.title}>{columnDef.title}</TableHeaderColumn>
                                })
                            }
                            {
                                this.props.config.actions.length &&
                                <TableHeaderColumn>{'Acciones'}</TableHeaderColumn>
                            }
                        </TableRow>
                    </TableHeader>
                    <TableBody displayRowCheckbox={this.props.config.selectable}>
                        {
                            this.props.data.map(dataItem => {
                                return <TableRow key={dataItem.id || 'id' + Math.random()}>
                                    {this.buildRowColumns(dataItem, this.props.config.columns)}
                                    this.props.config.actions.length &&
                                <TableRowColumn>

                                        <IconMenu
                                            iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
                                            anchorOrigin={{ horizontal: 'left', vertical: 'top' }}
                                            targetOrigin={{ horizontal: 'left', vertical: 'top' }}
                                        >
                                            {this.buildOptionsMenu(dataItem, this.props.config.actionsProperty)}
                                        </IconMenu>
                                    </TableRowColumn>
                                </TableRow>
                            })
                        }
                    </TableBody>
                </Table>
            </Paper>

        );
    }
}

export default DataTable;
