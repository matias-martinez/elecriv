import React, { Component } from 'react';

class ErrorImg extends Component {

    constructor(props) {
        super(props);
        this.styles = {
            height: '100%',
            display: 'inline-block',
            img: {
                verticalAlign: 'middle',
                height: '240px'
            },
            divTxt: {
                display: 'inline-grid',
                textAlign: 'center',
                marginTop: '15px'
            },
            txt: {
                fontSize: 16,
                color: 'black'
            }
        }
    }



    render() {

        return (
            <div style={this.styles}>
                <div>
                    <img src={"../../images/" + this.props.image} style={this.styles.img} alt="Error" />
                </div>
                <div style={this.styles.divTxt}>
                    <span style={this.styles.txt}>{this.props.message}</span>
                    <span style={this.styles.txt}>{this.props.secondMessage}</span>
                </div>
            </div>
        );
        
    }
}

export default ErrorImg;
