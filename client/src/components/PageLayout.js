import React, { Component } from 'react';
import Menu from './Menu';
import Logged from './Logged';
import ErrorImg from './common/ErrorImg';
import { CircularProgress } from 'material-ui';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import FlatButton from 'material-ui/FlatButton';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';
import { typography } from 'material-ui/styles';
import { black } from 'material-ui/styles/colors';
import { extend } from 'lodash';
import withWidth, { LARGE } from 'material-ui/utils/withWidth';

// REDUX Imports - Always there.
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import { actionCreators as SessionActions } from '../ducks/SessionDuck';

/**
 * This component represents the structure of the page holding its
 * main components: header, menu and content.
 * The component supports two modes, one for users that are logged and
 * one for users that are not yet logged so when a page is created
 * it is mandatory to indicate if that page is meant for logged users
 * or not.
 * The content is filled with the component's children, which means
 * it is not meant to be extended but composed.
 */


class PageLayout extends Component {

    constructor(props) {
        super(props);
        this.state = {
            open: props.width === LARGE
        };
        this.styles = {
            navigation: {
                fontWeight: typography.fontWeightBold,
                color: black,
                paddingBottom: 30,
                fontSize: 20,
                display: 'block'
            },
            paper: {
                padding: 30
            },
            clear: {
                clear: 'both'
            }
        };
        window.scrollTo(0, 0);
        this.login = this.login.bind(this);
        this.logout = this.logout.bind(this);
        this.props.requiresAuthentication && !this.props.logged && this.props.navigate('/');        
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.width !== nextProps.width) {
            this.setState({open: nextProps.width === LARGE});
            nextProps.width === LARGE && window.dispatchEvent(new Event('resize'));
        }        
    }

    toggleMenu = () => this.setState({open: !this.state.open});

    closeMenu = () => this.setState({open: false});

    login = () => {
        this.props.navigate('/login');
    };

    logout = () => {
        this.props.sessionActions.logout('/');
    };

    renderWithLoaderAndErrorHandler(component) {
        if (this.props.isLoading) return this.renderLoader();
        if (this.props.hasError) return this.renderError();
        return component;
    }

    renderError() {
        return (
            <div style={{textAlign: 'center', paddingBottom: '38px', paddingTop: '40px'}}>
                <ErrorImg message="There was an error."
                          secondMessage="Please try again in a couple of minutes"
                          image="emptystate-graphic.png"/>
            </div>
        );
    }

    renderLoader() {
        return (
            <div style={{textAlign: 'center'}}>
                <CircularProgress style={{padding: 30}}/>
            </div>
        );
    }
    render() {

        const paddingLeftDrawerOpen = 266;
        const styles = {
            header: {
                position: 'fixed',
            },
            container: {
                paddingLeft: this.state.open && this.props.width === LARGE ? paddingLeftDrawerOpen : 40
            }
        };
        const hasValidAuth = !this.props.requiresAuthentication || this.props.logged;
        const hasValidClient = !this.props.requiresClient || this.props.clientId !== null;
        const shouldShowContent = hasValidAuth && hasValidClient;

        extend(styles.header, this.state.appBar);
        extend(styles.container, this.state.appBar);

        return (

            <div>
                <div>
                    <AppBar
                        style={styles.header}
                        title={"ElecRiv"}
                        showMenuIconButton={true}
                        iconElementLeft={<IconButton><NavigationMenu color={black}/></IconButton>}
                        onLeftIconButtonTouchTap={this.toggleMenu}
                        iconElementRight={this.props.logged ? <Logged doOnLogout={this.logout}/>
                            :  <FlatButton onTouchTap={this.login} label="Login" />}
                    />
                    <div>
                        <Menu {...this.props} open={this.state.open} active={this.props.location.pathname}/>
                    </div>
                </div>
                <div className="container-fluid main-container" style={styles.container}>
                    <h2 style={this.styles.navigation}>
                        {this.props.mainNavigation &&
                        <span><span style={{color: '#767676'}}>{this.props.mainNavigation} </span>| </span>}
                        {this.props.navigation}</h2>

                    { shouldShowContent ?
                        this.renderWithLoaderAndErrorHandler(this.props.children)
                        : <p>User is not allowed to access this page.</p> }
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        logged: state.session.logged,
        client: state.session.clientId
    }
}

function mapDispatchToProps(dispatch) {
    return {
        navigate: bindActionCreators(push, dispatch),
        sessionActions: bindActionCreators(SessionActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(withWidth()(PageLayout));
