import React, { Component } from 'react';
import Drawer from 'material-ui/Drawer';
import { white } from 'material-ui/styles/colors';
import { List, ListItem } from 'material-ui/List';
import { startsWith } from 'lodash';
import MenuService from '../utils/MenuService';

class Menu extends Component {
    styles = {
        logo: {
            height: '90%',
            maxWidth: '150px',
            verticalAlign: 'middle'
        },
        menuItem: {
            color: white,
            fontSize: 14,
            fontWeight: 'normal',
            opacity: 0.7,
            padding: '15px 0'
        },
        menuItemActive: {
            color: white,
            fontSize: 14,
            fontWeight: 'bold',
            padding: '2px 0'
        },
        menuListItem: {
            color: white,
            fontSize: 14,
            fontWeight: 'normal',
            opacity: 0.7,
            padding: '2px 0'
        },
        menuListItemActive: {
            color: white,
            fontSize: 14,
            fontWeight: 'bold',
            padding: '2px 0'
        },
        list: {
            color: white,
            fontSize: 15,
            fontWeight: 'normal',
        },
        divider: {
            opacity: 0.7,
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            open: false
        };
        this.buildMenu = this.buildMenu.bind(this);
    }

    handleMenuItemClicked = (path) => {
        this.props.history.push(path);
    };

    handleMenuListClicked = (open, param, path, parent) => {
        let state = {};
        state[param] = !open;
        this.setState(state);
        if (!open && !(startsWith(window.location.pathname, parent))) {
            this.props.history.push(path);
            this.props.onItemTouchTap();
        }
    };


    buildMenu() {

        return MenuService.getMenuItems().map(item => {
            return (<ListItem
                key={item.title}
                containerElement="div"
                style={this.props.active === item.target ? this.styles.menuItemActive : this.styles.menuItem}
                onClick={() => this.handleMenuItemClicked(item.target)}
                leftIcon={item.icon || null}>
                {item.title}
            </ListItem>);
        });

    }

    render() {
        return (
            <Drawer
                containerStyle={{ boxShadow: 'none', top: 57 }}
                docked={true}
                width={230}
                open={this.props.open}
                onRequestChange={(open) => this.props.onItemTouchTap()}>
                <List>
                    {this.buildMenu()}
                </List>

            </Drawer>
        );
    }
}

export default Menu;
