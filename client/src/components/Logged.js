import React, { Component } from 'react';
import IconButton from 'material-ui/IconButton';
import CircularProgress from 'material-ui/CircularProgress';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import AuthService from '../utils/AuthService';
import { Paper } from 'material-ui';
import { Modal } from 'react-overlays';


const buttonStyle = {
    borderLeft: '1px solid rgba(213,213,213,0.5)',
    color: '#E13725',
    width: '100%',
    height: 50,
    justifyContent: 'center',
    borderTop: '1px solid rgba(213,213,213,0.5)',
    fontSize: 14,
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer'
};

const menuStyle = {
    position: 'absolute',
    right: 10
};


class Logged extends Component {

    constructor(props) {
        super(props);
        this.state = {
            signingOut: false,
            menuClosed: true
        };
        this.logout = this.logout.bind(this);
        this.handleButtonClick = this.handleButtonClick.bind(this);
    }

    handleButtonClick() {
        this.setState({
            menuClosed: !this.state.menuClosed
        });
    }


    logout() {
        this.setState = { signingOut: true };
        AuthService.logout().then(() => { this.props.doOnLogout(); });
    }

    render() {
        const user = AuthService.getSession();
        
        return (
            <div>
                <IconButton onClick={this.handleButtonClick}>
                    {this.signingOut ?
                        <CircularProgress size={20} /> : <MoreVertIcon color={'black'} />
                    }
                </IconButton>
                {!this.state.menuClosed &&
                    <Modal show={true} style={{ position: 'absolute', top: 5, right: 10, display: 'block', zIndex: 1111 }}
                        onHide={this.handleButtonClick}>
                        <div style={menuStyle}>
                            <Paper >
                                <div style={{ display: 'flex', minWidth: 250 }}>
                                    <div>
                                        <img style={{ width: 59, height: 59, borderRadius: '50%', margin: '12px 14px 12px 22px' }}
                                            src={user.picture} alt="" />
                                    </div>
                                    <div style={{ paddingRight: 20, display: 'flex', alignItems: 'center' }}>
                                        <div>
                                            <div style={{ fontSize: 13, fontWeight: 'bold', color: '#212121' }}>
                                                {user.name}
                                            </div>
                                            <div style={{ fontSize: 12, marginTop: 3, color: '#212121' }}>
                                                {user.email}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style={{ display: 'flex' }}>
                                    <div style={buttonStyle}
                                        onClick={this.logout}>
                                        Logout
                                    </div>
                                </div>
                            </Paper>
                        </div>

                    </Modal>

                }

            </div>
        );
    }
}

export default Logged;
