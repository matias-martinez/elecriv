import API from '../utils/API';

const KEY = 'clicklab/servicios'
const GET_SERVICES = KEY + '/GET_SERVICES';
const GET_SERVICES_REQUESTED = GET_SERVICES + '_REQUESTED';
const GET_SERVICES_DONE = GET_SERVICES + '_DONE';
const GET_SERVICES_FAILED = GET_SERVICES + '_FAILED';


const DEFAULT_STATE = {
    'isFetching': false,
    'hasError': false,
    'services': []
};

/**
 * Action Creator for getting services
 */
const getAll = () => (dispatch) => {
    dispatch({ type: GET_SERVICES_REQUESTED });  
    const client = new API();
    return client.call('GET', 'servicios/').then((res)=>{
        const services = res.data.map((dataItem)=>{
            dataItem.actions=[{title: 'Edit'}]; // We should do this somewhere else. Maybe in a selector??
            return dataItem;
        });
        dispatch({type:GET_SERVICES_DONE, payload: services});    
    })
    .catch((error)=>dispatch({type: GET_SERVICES_FAILED, payload: error}));
};


const reducer = (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case GET_SERVICES_REQUESTED:
            return {...state, isLoading: true, error: false};            
        case GET_SERVICES_DONE:
            return {...state, isLoading: false, services: action.payload};
        case GET_SERVICES_FAILED:
            return {...state, isLoading: false, error: true, errorDetail: action.payload};
        default: {
            return state;
        }
    }
};


export const actionCreators = {
    getAll
};

export default reducer;