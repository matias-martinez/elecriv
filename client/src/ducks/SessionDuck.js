import AuthService from '../utils/AuthService';
import { push } from 'react-router-redux';

const KEY = 'clicklab/session'
const SESSION_LOGOUT = KEY + '/SESSION_LOGOUT';
const SESSION_LOGIN = KEY + '/SESSION_LOGIN';
const SESSION_LOGIN_REQUESTED = SESSION_LOGIN + '_PENDING';
const SESSION_LOGIN_REJECTED = SESSION_LOGIN + '_REJECTED';
const SESSION_LOGIN_FULFILLED = SESSION_LOGIN + '_FULFILLED';

const DEFAULT_STATE = {
    'logged': false,
    'isFetching': false,
    'hasError': false,
    ...AuthService.getSessionInfo()
};

/**
 * Action Creator for login via google
 * @param {*} googleToken
 */
const login = (username, pass) => (dispatch) => {
    dispatch({ type: SESSION_LOGIN_REQUESTED });
    return AuthService.login(username, pass)
        .then( response => {
            dispatch({
                type: SESSION_LOGIN_FULFILLED,
                payload: response
            });
           dispatch(push('/home'));
        })
        .catch(error => dispatch({ type: SESSION_LOGIN_REJECTED, payload: error }));
};
/**
 * Action Creator for checking session
 */
const sessionCheck = () => (dispatch) => {
    if (AuthService.isLoggedIn()) {
        dispatch(push('/home'));        
    } 
};



/**
 * Action Creator for logout
 */
const logout = () => (dispatch) => {
    dispatch({ type: SESSION_LOGOUT });
    dispatch(push('/login'));
};

const reducer = (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case SESSION_LOGOUT: {
            const ns = {
                ...state, isFetching: false, logged: false, session: null,
                userName: null, userEmail: null, userPicture: null, hasError: false
            };
            AuthService.logout();
            return ns;
        }
        case SESSION_LOGIN_REQUESTED: {
            return {
                ...state, isFetching: true, logged: false, session: null,
                userName: null, userEmail: null, userPicture: null, hasError: false
            }
        }
        case SESSION_LOGIN_FULFILLED: {
            const ns = {
                ...state,
                ...action.payload,
                isFetching: false,
                logged: true,
                hasError: false
            };
            return ns;
        }
        case SESSION_LOGIN_REJECTED: {
            return {
                ...state,
                isFetching: false,
                logged: false,
                session: false,
                userName: null,
                userEmail: null,
                userPicture: null,
                hasError: true
            }
        }
        default: {
            return state;
        }
    }
};


export const actionCreators = {
    login,
    logout,
    sessionCheck
};

export default reducer;