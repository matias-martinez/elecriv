# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from models import servicio, cliente, cuadrilla

# Register your models here.
admin.site.register(cliente.Cliente)
admin.site.register(servicio.Servicio)
admin.site.register(cuadrilla.Cuadrilla)
