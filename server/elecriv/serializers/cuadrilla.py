from elecriv.models import Cuadrilla
from rest_framework import serializers


class CuadrillaSerializer(serializers.ModelSerializer):
  
    class Meta:
        model = Cuadrilla
        fields = ('id', 'nombre', 'miembros')
