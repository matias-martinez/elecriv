from elecriv.models import Servicio
from elecriv.serializers import ClienteSerializer, CuadrillaSerializer
from rest_framework import serializers


class ServicioListSerializer(serializers.ModelSerializer):
    cliente = ClienteSerializer()
    cuadrilla = CuadrillaSerializer()

    class Meta:
        model = Servicio
        fields = ('id', 'fecha_solicitud', 'fecha_inicio', 'cuadrilla',
                  'fecha_fin', 'descripcion', 'estado', 'cliente')


class ServicioCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Servicio
        fields = ('id', 'fecha_solicitud', 'fecha_inicio',
                  'fecha_fin', 'descripcion', 'estado', 'cliente')
