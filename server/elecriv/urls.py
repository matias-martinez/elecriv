from django.conf.urls import url

from views import cliente, servicio, cuadrilla

urlpatterns = [
    url(r'^clientes/$', cliente.ClienteList.as_view()),
    url(r'^clientes/(?P<pk>[0-9]+)/$', cliente.ClienteDetail.as_view()),
    url(r'^servicios/$', servicio.ServicioListCreateView.as_view()),
    url(r'^servicios/(?P<pk>[0-9]+)/$', servicio.ServicioDetailView.as_view()),
    url(r'^cuadrillas/$', cuadrilla.CuadrillaList.as_view()),
    url(r'^cuadrillas/(?P<pk>[0-9]+)/$', cuadrilla.CuadrillaDetail.as_view()),
]
