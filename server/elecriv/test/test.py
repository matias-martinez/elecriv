# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase

from elecriv.models.cliente import Cliente

class ClienteTestCase(TestCase):
    '''
        Tests for Client Model
    '''
    def setUp(self):
        Cliente.objects.create(nombre='pae', direccion='514', telefono='123')
        Cliente.objects.create(nombre='ypf')

    def test_cliente_contacto(self):
        pae = Cliente.objects.get(nombre='pae')
        self.assertEqual(pae.contacto(), 'Direccion: 514 - Tel: 123')
    