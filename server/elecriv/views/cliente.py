from elecriv.models import Cliente
from elecriv.serializers import ClienteSerializer
from rest_framework import generics, filters
from rest_framework.permissions import IsAuthenticated

class ClienteList(generics.ListCreateAPIView):
    queryset = Cliente.objects.all()
    serializer_class = ClienteSerializer
    filter_backends = (filters.OrderingFilter,)
    permission_classes = (IsAuthenticated,)

class ClienteDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Cliente.objects.all()
    serializer_class = ClienteSerializer
    permission_classes = (IsAuthenticated,)
