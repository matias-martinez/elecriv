from elecriv.models import Cuadrilla
from elecriv.serializers import CuadrillaSerializer
from rest_framework import generics, filters
from rest_framework.permissions import IsAuthenticated

class CuadrillaList(generics.ListCreateAPIView):
    queryset = Cuadrilla.objects.all()
    serializer_class = CuadrillaSerializer
    filter_backends = (filters.OrderingFilter,)
    permission_classes = (IsAuthenticated,)

class CuadrillaDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Cuadrilla.objects.all()
    serializer_class = CuadrillaSerializer
    permission_classes = (IsAuthenticated,)
