from elecriv.models import Servicio
from elecriv.serializers.servicio import ServicioListSerializer, ServicioCreateSerializer
from rest_framework import generics, filters
from rest_framework.permissions import IsAuthenticated

class ServicioListCreateView(generics.ListCreateAPIView):
    queryset = Servicio.objects.all()
    serializer_class = ServicioListSerializer
    filter_backends = (filters.OrderingFilter,)
    permission_classes = (IsAuthenticated,)

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return ServicioCreateSerializer
        return ServicioListSerializer

class ServicioDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Servicio.objects.all()
    serializer_class = ServicioListSerializer
    permission_classes = (IsAuthenticated,)


