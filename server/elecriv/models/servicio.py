from django.db import models
from elecriv.models.cliente import Cliente
from elecriv.models.cuadrilla import Cuadrilla

class Servicio(models.Model):
    '''
    Representa un servicio. Solicitado por un cliente.
    Sujeto a presupuesto.
    '''

    ESTADO_CHOICES = (
        ('NUEVO', 'Nuevo'),
        ('ACEPTADO', 'Aceptado'),
        ('EN_PROGRESO', 'En Progreso'),
        ('FINALIZADO', 'Finalizado')
    )

    fecha_solicitud = models.DateTimeField(
        blank=False, null=False, auto_now_add=False)
    fecha_inicio = models.DateTimeField(blank=True, null=True)
    fecha_fin = models.DateTimeField(blank=True, null=True)
    descripcion = models.TextField(max_length=5000, blank=True, null=True)
    estado = models.CharField(
        max_length=15,
        choices=ESTADO_CHOICES,
        default='NUEVO',
    )
    cliente = models.ForeignKey(Cliente, null=False)
    cuadrilla = models.ForeignKey(Cuadrilla, null=True)
