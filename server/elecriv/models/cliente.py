from django.db import models

class Cliente(models.Model):
    '''
        Representa un cliente. Puede ser una persona juridica o una fisica
    '''
    nombre = models.TextField(max_length=400)
    direccion = models.TextField(max_length=600)
    telefono = models.TextField(max_length=40)

    def contacto(self):
        '''Sort of toString for contact info
        '''
        return 'Direccion: {} - Tel: {}'.format(self.direccion, self.telefono)
