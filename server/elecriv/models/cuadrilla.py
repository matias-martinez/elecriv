from django.db import models
from django.contrib.auth.models import User

class Cuadrilla(models.Model):
    '''
    Representa una cuadrilla de trabajo.
    '''

    miembros = models.ManyToManyField(User)
    nombre = models.TextField(max_length=400)
