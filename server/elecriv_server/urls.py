"""elecriv URL Configuration
"""
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework.documentation import include_docs_urls
from rest_framework.schemas import get_schema_view

schema_view = get_schema_view(title='Pastebin API')


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^docs/', include_docs_urls(title='ElecRIV API service')),
    url(r'^drf/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/', include('elecriv.urls')),
    url(r'^api/auth/', include('core.auth2.urls')),
    url(r'^schema/$', schema_view)

]
