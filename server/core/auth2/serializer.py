from django.contrib.auth.models import User, Group
from django.contrib.auth import authenticate

from rest_framework import serializers

class LoginSerializer(serializers.Serializer):

    user = serializers.CharField(required=False, allow_blank=True)
    password = serializers.CharField(style={'input_type': 'password'})

    def validate(self, data):
        """
            try to authenticate user with request data
        """
        try:
            find_user = User.objects.get(username__iexact=data['user'])
            user = authenticate(username=find_user.username, password=data['password'])
        except :
            raise serializers.ValidationError("User does not exist")
        if user is None:
            raise serializers.ValidationError("Authentication error")
        return user
