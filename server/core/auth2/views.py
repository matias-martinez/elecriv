"""
    Views For Session Stuff
"""
import re
from django.contrib.auth import login, logout
from django.contrib.auth.models import User

from rest_framework.generics import GenericAPIView
from rest_framework.views import APIView
from rest_framework.response import Response

from core.auth2.serializer import LoginSerializer
from core.auth2.utils.csrf_exempt_session_authentication import CsrfExemptSessionAuthentication
from django.views.decorators.csrf import csrf_exempt

class LoginView(GenericAPIView):

    serializer_class = LoginSerializer
    authentication_classes = (CsrfExemptSessionAuthentication,)

    @csrf_exempt
    def post(self, request):
        """
            if request.data is valid, logs user in and returns his info
        """
        self.request = request
        self.serializer = self.get_serializer(data=self.request.data)
        self.serializer.is_valid(raise_exception=True)
        user = self.serializer.validated_data
        login(self.request, user)

        content = {
            'email': user.email,
            'role': user.groups.values_list('name', flat=True)[0],
            'id': user.id,
            'first_name': user.first_name,
            'last_name': user.last_name
        }
        return Response(status=200, data=content)


class LogoutView(APIView):
    """
        Logs an user out
    """
    authentication_classes = (CsrfExemptSessionAuthentication,)

    def post(self, request):
        """
            logs user out, delete session and response with a vary cookie header
        """
        logout(request)
        content = {'message': 'Success'}
        return Response(status=200, data=content)


class SessionView(GenericAPIView):
    """
        To be used by clients for session sync purposes
    """

    def get(self, request):
        """
            check if user have a valid session (is authenticated)
            and returns a response with his info
        """
        user = request.user
        if user.is_authenticated():
            content = {
                'email': user.email,
                'role': user.groups.values_list('name', flat=True)[0],
                'id': user.id,
                'first_name': user.first_name,
                'last_name': user.last_name
            }
            return Response(status=200, data=content)
        else:
            return Response(
                {'status': 'Unauthorized',
                 'message': 'Invalid Session'
                 },
                status=401)


class ValidateEmailView(GenericAPIView):
    """
        Utility for validating if email is available
    """
    def get(self, request):
        email = request.query_params['email']
        if email != '' and not re.match(r'^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$', email):
            return Response(status=400, data={'error': 'Invalid email'})
         # I'm trying to get a user. If I can, user exist.
        user = User.objects.filter(username__iexact=email)
        user_len = len(user)
        if user_len > 0:
            return Response(status=200, data={'exists': 'true'})
        # user doesn't exist
        return Response(status=200, data={'exists': 'false'})
