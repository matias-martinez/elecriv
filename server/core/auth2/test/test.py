# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework.test import APIClient


class LogInTest(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'email': 'test@test.com',
            'password': 'secret'}
        User.objects.create_user(**self.credentials)
    def test_login(self):
        # send login data
        client = APIClient()
        response = client.post('/api/auth/login/', self.credentials)
        # should be logged in now
        self.assertEquals(response.data['email'], 'test@test.com')
    