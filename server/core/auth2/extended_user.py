from django.contrib.auth.models import User


class ExtendedUser(User):
    objects = User.objects.all()

    class Meta:
        proxy = True

    def get_basic_data(self):
        return {
            'email': self.email,
            'role': self.groups.values_list('name', flat=True),
            'id': self.id,
            'first_name': self.first_name,
            'last_name': self.last_name,
        }
