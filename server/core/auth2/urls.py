from django.conf.urls import url
from views import LoginView, LogoutView, SessionView

from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    url(r'login/$', LoginView.as_view()),
    url(r'logout/$', LogoutView.as_view()),
    url(r'session/$', SessionView.as_view()),
]
